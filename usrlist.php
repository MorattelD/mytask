<?php
require_once('inc/config.php');

$stmt = $db->prepare($q_usrselect);
$stmt->execute();
$users= $stmt->fetchAll();
?>


<span class="btn-back hide-for-small-only" title="Back">
  <a href="index.php"><i class="fa fa-arrow-left fa-3x" aria-hidden="true"></i></a>
</span>

  <ul class="list">

  <li class="row">

  </li>
        <?php foreach ($users as $row): ?>

          <li class="row list-usr">

            <?php
              $usrimg = "assets/img/" . $row['id'] . ".jpg";
              if (!file_exists($usrimg)){
                $usrimg = "assets/img/0.jpg";
              }
            ?>

            <a class="" href="usredit.php?id=<?php echo $row['id']; ?>">
              <span>
                <img class="header-usermenu-img" src="<?php echo $usrimg;?>"/>
              </span>
              <span class=" list-usr-name">
                <?php echo $row['name']; ?>
              </span>


            </a>

              <span class=" list-usr-delete" data-id="<?php echo $row['id']; ?>">
                <a href="#">
                  <i class="fa fa-trash-o"></i>
                </a>
              </span>

          </li>

        <?php endForeach; ?>
    </ul>

<a id="btn-add" href="usredit.php"><i class="fa fa-plus fa-2x"></i></a>
