-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `tasklist`;
CREATE DATABASE `tasklist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tasklist`;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('open','close') NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `due` date DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `done_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `assigned_to` (`assigned_to`),
  KEY `done_by` (`done_by`),
  CONSTRAINT `task_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `task_ibfk_2` FOREIGN KEY (`assigned_to`) REFERENCES `user` (`id`),
  CONSTRAINT `task_ibfk_3` FOREIGN KEY (`done_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `task` (`id`, `status`, `description`, `date`, `due`, `priority`, `created_by`, `assigned_to`, `done_by`) VALUES
(72,	'open',	'etst',	'2017-06-14',	'2017-06-15',	0,	NULL,	NULL,	NULL),
(73,	'open',	'setset',	'2017-06-14',	'2017-06-15',	0,	NULL,	NULL,	NULL),
(74,	'open',	'tsdfasd',	'2017-06-14',	'2017-06-15',	0,	NULL,	NULL,	NULL),
(75,	'open',	'sdfasdf',	'2017-06-14',	'2017-06-15',	0,	NULL,	NULL,	NULL),
(76,	'open',	'sdfas',	'2017-06-14',	'2017-06-15',	0,	NULL,	NULL,	NULL),
(109,	'open',	'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',	'2017-06-22',	'2017-06-23',	10,	1,	1,	NULL),
(110,	'close',	'222222222',	'2017-06-22',	'2017-06-23',	0,	1,	1,	1),
(111,	'open',	'33333333',	'2017-06-22',	'2017-09-23',	0,	1,	9,	NULL),
(112,	'open',	'444444444',	'2017-06-22',	'2017-07-23',	0,	1,	8,	NULL),
(113,	'close',	'55555555',	'2017-06-22',	'2017-06-23',	0,	1,	1,	1),
(114,	'open',	'666666666',	'2017-06-22',	'2017-06-23',	0,	1,	1,	NULL),
(115,	'open',	'77777777',	'2017-06-22',	'2017-06-23',	7,	1,	1,	NULL),
(116,	'open',	'88888888',	'2017-06-22',	'2017-06-23',	8,	1,	1,	NULL),
(118,	'open',	'99999999999999999',	'2017-06-24',	'2017-06-25',	3,	1,	9,	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1,	'Damien Morattel',	'user',	'pass'),
(8,	'test',	'test',	'test'),
(9,	'test1',	'test1',	'test1');

-- 2017-06-24 15:52:03
