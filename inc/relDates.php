<?php
//+ Maigret Aurélien
//@ https://www.dewep.net
// Modified by Damien Morattel
function getRelativeTime($date)
{
    $date_a_comparer = new DateTime($date);
    $date_actuelle = (new DateTime("now"))->format('Y-m-d');
    $date_actuelle = new DateTime($date_actuelle);

    $intervalle = $date_a_comparer->diff($date_actuelle);

   $ans = $intervalle->format('%y');
   $mois = $intervalle->format('%m');
   $jours = $intervalle->format('%d');


   if ($date_a_comparer > $date_actuelle)
   {
       $prefixe = 'dans ';
   }
   else
   {
       $prefixe = 'il y a ';
   }

   if($jours == 0){
     $prefixe = '';
   }


   if ($ans != 0)
   {
       $relative_date = $prefixe . $ans . ' an' . (($ans > 1) ? 's' : '');
      if ($mois >= 6) $relative_date .= ' et demi';
   }
   elseif ($mois != 0)
   {
       $relative_date = $prefixe . $mois . ' mois';
       if ($jours >= 15) $relative_date .= ' et demi';
   }
   elseif ($jours != 0)
   {
       $relative_date = $prefixe . $jours . ' jour' . (($jours > 1) ? 's' : '');
       if($jours ==1){
         $relative_date=($date_a_comparer > $date_actuelle)?'demain':'hier';
       }
   }
   else
   {
       $relative_date = $prefixe . "aujourd'hui";
   }

   return $relative_date;
}
 ?>
