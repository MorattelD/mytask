<?php
  if(session_status() == PHP_SESSION_NONE){
    session_start();
  }
  $q_select="SELECT task.*, creator.name as creator, assignee.name as assignee, executor.name as executor FROM task INNER JOIN user creator ON task.created_by = creator.id LEFT JOIN user assignee ON task.assigned_to = assignee.id LEFT JOIN user executor ON task.done_by = executor.id";
  $q_edit="SELECT * FROM task WHERE id = ?";
  $q_delete="DELETE FROM task WHERE id = ?";
  $q_update="UPDATE task SET description=?, due=?, priority=?, created_by=?, assigned_to=? WHERE id = ?";
  $q_insert="INSERT INTO task(description,date,due,priority,created_by,assigned_to) VALUES(?,?,?,?,?,?)";
  $q_done="UPDATE task SET status=?, done_by=? WHERE id = ?";
  $q_user="SELECT name FROM user WHERE id = ?";
  $q_login="SELECT id FROM user WHERE email = ? AND password = ?";

  $q_usrselect="SELECT * FROM user";
  $q_usrdelete="DELETE FROM user WHERE id = ?";
  $q_usredit="SELECT * FROM user WHERE id = ?";
  $q_usrupdate="UPDATE user SET name=?, email=?, password=? WHERE id = ?";
  $q_usrupdate2="UPDATE user SET name=?, email=? WHERE id = ?";
  $q_usrpass="SELECT password FROM user WHERE id = ?";
  $q_usrinsert="INSERT INTO user(name,email,password) VALUES(?,?,?)";


  $db = new PDO('mysql:host=localhost;dbname=tasklist;charset=utf8mb4','root','pass');

   require_once('relDates.php');
   require_once('security.php');

 ?>
