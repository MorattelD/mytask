<!doctype html>

<?php
require_once('inc/config.php');

$modify = FALSE;

$current_date = date('Y-m-d', strtotime('+1 day'));


if(isset($_REQUEST['id'])){
  $idd = $_REQUEST['id'];
  $modify = TRUE;
  $stmt = $db->prepare($q_edit);
  $stmt->execute(array($idd));
  $tasks = $stmt->fetchAll();


  $due = $tasks[0]['due'];
  $description = $tasks[0]['description'];
  $priority = $tasks[0]['priority'];
  $assigned_to = $tasks[0]['assigned_to'];
}
?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>

  <main class="off-canvas-content main" data-off-canvas>

    <span class="btn-back hide-for-small-only" title="Back">
      <a href="index.php"><i class="fa fa-arrow-left fa-3x" aria-hidden="true"></i></a>
    </span>

        <form class="form-edit" method="post" action="update.php">
          <ul>
              <input id="id" name="id" type="hidden" value="<?php echo $modify?$idd:''; ?>"/>
            <li class="row medium-6 large-4 columns">
              <label for="due">Due date</label>
              <input class="form-edit-input" name="due" id="due" type="date" value="<?php echo $modify?$due:$current_date; ?>"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="priority">Priority</label>
              <input class="form-edit-input" name="priority"  id="priority" value="<?php echo $modify?$priority:'0'; ?>" min="0" max="10" place type="number"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="assigned_to">Assigned to</label>
                <?php
                  require_once('template/usrdropdown.php');
                ?>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="description">Description</label>
              <textarea class="form-edit-input" name="description"  id="description" autofocus required><?php echo $modify?$description:''; ?></textarea>
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>


      </main>
      </div>
      <?php require_once('template/footer.php'); ?>
    </body>
</html>
