<!doctype html>

<?php
require_once('inc/config.php');

$modify = FALSE;

if(isset($_REQUEST['id'])){
  $idd = $_REQUEST['id'];
  $modify = TRUE;
  $stmt = $db->prepare($q_usredit);
  $stmt->execute(array($idd));
  $user = $stmt->fetch();


  $name = $user['name'];
  $mail = $user['email'];
}
?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>

  <main class="off-canvas-content main" data-off-canvas>

    <span class="btn-back hide-for-small-only" title="Back">
      <a href="index.php?action=usr"><i class="fa fa-arrow-left fa-3x" aria-hidden="true"></i></a>
      </span>

        <form class="form-edit" method="post" enctype="multipart/form-data" action="update.php">
          <ul>
              <input id="id" name="id" type="hidden" value="<?php echo $modify?$idd:''; ?>"/>
              <input id="usr" name="usr" type="hidden" value=""/>

            <li class="row medium-6 large-4 columns">
              <label for="due">Name</label>
              <input class="form-edit-input" name="name" id="name" type="text" autofocus required value="<?php echo $modify?$name:''; ?>"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="mail">Mail</label>
              <input class="form-edit-input" name="mail" id="mail" required value="<?php echo $modify?$mail:''; ?>" type="text"/>
            </li>
            <?php if($modify) : ?>
              <li class="row medium-6 large-4 columns">
                <label for="pass1">Current password</label>
                <input class="form-edit-input" name="pass1" type="password" required  id="pass1" ?>
              </li>
            <?php endif; ?>
            <li class="row medium-6 large-4 columns">
              <label for="pass2">Password</label>
              <input class="form-edit-input" name="pass2" type="password"  id="pass2" ?>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="pass3">Repeat password</label>
              <input class="form-edit-input" name="pass3" type="password"  id="pass3" ?>
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>


      </main>
      </div>
      <?php require_once('template/footer.php'); ?>
    </body>
</html>
