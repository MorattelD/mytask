<!doctype html>

<?php require_once('inc/config.php');

if(!isset($_SESSION['filter'])){
  $_SESSION['filter']='all';
} if (!isset($_SESSION['status'])) {
  $_SESSION['status']='open';
} if (!isset($_SESSION['sort'])) {
  $_SESSION['sort']='date';
} if (!isset($_SESSION['order'])) {
  $_SESSION['order']='asc';
}

?>



<html class="no-js" lang="en">
  <?php require_once('template/head.php'); ?>
  <body>
    <?php require_once('template/header.php'); ?>


<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>
  <main class="off-canvas-content main" data-off-canvas>
    <?php
      if(isset($_REQUEST['action'])){
        switch ($_REQUEST['action']) {
          case 'usr':
            require_once('usrlist.php');
            break;

          case 'cv':
            require_once('cv.html');
            break;

            case 'about':
              require_once('about.html');
              break;

              case 'manual':
                require_once('manual.php');
                break;
          default:
            require_once('tasklist.php');
            break;
        }
      }else{
        require_once('tasklist.php');
      }
    ?>
  </main>
</div>
    <?php require_once('template/footer.php'); ?>
  </body>
</html>
