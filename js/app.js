$(document).foundation();

$(document).on('click', '.list-task-delete a', {}, function(e) {
  $this = $(this).parents('.list-task-delete');
  $.ajax({
    url: "delete.php?id=" + $this.data('id')
  }).done(function() {
    $this.parents(".list-task").remove();
  });
});

$(document).on('click', '.list-task-status a', {}, function(e) {
  $this = $(this).parents('.list-task-status');
  $done = $this.data('status');
  $.ajax({
    url: "done.php?id=" + $this.data('id') + "&status=" + $done
  }).done(function() {
    $this.data('status', $done == 1 ? '' : 1);
    $this.parents(".list-task").toggleClass('done');
    $this.children('a').children('i').toggleClass('fa-check-square-o');
    $this.children('a').children('i').toggleClass('fa-square-o');
  });
});

$(document).on('click', '.list-usr-delete', {}, function(e) {
  $this = $(this);
  $.ajax({
    url: "delete.php?id=" + $this.data('id') + "&usr"
  }).done(function() {
    $this.parents(".list-usr").remove();
  });
});

$filter = 'filter=all';
$sort = 'sort=date';
$order = 'order=asc';

$(document).on('click', '.sb-filter', {}, function(e) {
  $this = $(this);
  $filter = 'filter=' + $this.data('filter');
  $('.sb-filter').removeClass('selected');
  $this.addClass('selected');

  $.ajax({
    url: "tasklist.php?" + $filter
  }).done(function(data) {
    $('.off-canvas-content').html(data);
  });
});

$(document).on('click', '.sb-status', {}, function(e) {
  $this = $(this);
  $status = 'status=' + $this.data('status');
  $('.sb-status').removeClass('selected');
  $this.addClass('selected');

  $.ajax({
    url: "tasklist.php?" + $status
  }).done(function(data) {
    $('.off-canvas-content').html(data);
  });
});

$(document).on('click', '.sb-sort', {}, function(e) {
  $this = $(this);
  $sort = 'sort=' + $this.data('sort');
  $order = 'order=' + $this.data('order');
  $('.sb-sort i').removeClass('show');
  if($this.data('order') == 'asc'){
    $this.data('order', 'desc');
    $this.children('i').eq(0).addClass('show');
  }else{
    $this.data('order','asc');
    $this.children('i').eq(1).addClass('show');
  }
  $('.sb-sort').removeClass('selected');
  $this.addClass('selected');

  $.ajax({
    url: "tasklist.php?" + $sort + '&' + $order
  }).done(function(data) {
    $('.off-canvas-content').html(data);
  });
});
