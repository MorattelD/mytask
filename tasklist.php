<?php
require_once('inc/config.php');

$filter = '';
$sort = ' ORDER BY date ';
$order = 'asc';
$status = " WHERE task.status='open'";

if(isset($_REQUEST['filter'])){
  $_SESSION['filter'] = $_REQUEST['filter'];
}else if(isset($_REQUEST['sort']) && isset($_REQUEST['order'])){
  $_SESSION['sort'] = $_REQUEST['sort'];
  $_SESSION['order'] = $_REQUEST['order'];
}else if(isset($_REQUEST['status'])){
  $_SESSION['status'] = $_REQUEST['status'];
}


if(isset($_REQUEST['action']) && $_REQUEST['action']=='reset'){
  $_SESSION['status'] = 'open';
  $_SESSION['filter'] = 'all';
  $_SESSION['sort'] = 'date';
  $_SESSION['order'] = 'asc';
  header('Location: /index.php');
}


if(isset($_SESSION['status'])){

  switch ($_SESSION['status']) {
    case 'all':
      $status = " WHERE task.status='close' OR task.status='open'";
      break;

    case 'open':
      break;

    case 'close':
      $status = " WHERE task.status='close'";
      break;

    default:
      break;
  }

}


if(isset($_SESSION['filter'])){

  switch ($_SESSION['filter']) {
    case 'all':
      break;

    case 'created':
      $filter = " AND task.created_by=" . $id;
      break;

    case 'assigned':
      $filter = " AND task.assigned_to=" . $id;
      break;

    case 'created':
      $filter = " AND task.created_by=" . $id;
      break;

    case 'done':
      $filter = " AND task.done_by=" . $id;
      break;

    default:
      break;
  }

}

if(isset($_SESSION['sort']) && isset($_SESSION['order'])){
  switch ($_SESSION['sort']) {
    case 'assignee':
      $sort = ' ORDER BY assigned_to ';
      break;

    case 'creator':
      $sort = ' ORDER BY created_by ';
      break;

    case 'status':
      $sort = ' ORDER BY status ';
      break;

    case 'due':
      $sort = ' ORDER BY due ';
      break;

    case 'date':
      break;

    default:
      break;
  }

  $order = $_SESSION['order'];
}
$stmt = $db->prepare($q_select . $status . $filter . $sort . $order . ', priority desc');
$stmt->execute();
$tasks= $stmt->fetchAll();
?>

  <ul class="list">

  <li class="row">  </li>

        <?php foreach ($tasks as $row): ?>
          <?php $done = $row['status'] == 'close'; ?>

          <li class="row list-task <?php echo ($done)?'done':''; ?>">


            <div class=" list-task-status" data-id="<?php echo $row['id']; ?>" data-status="<?php echo $done; ?>">
              <a href="#">
                <?php echo ($done)?'<i class="fa fa-check-square-o"></i>':'<i class="fa fa-square-o"></i>'; ?>
              </a>
            </div>


            <div class="list-task-content">

            <a class="linkedit" href="edit.php?id=<?php echo $row['id']; ?>">

                <span class=" list-task-description">
                  <?php echo $row['description']; ?>
                </span>

            </a>


            <a class="linkedit scndrow" href="edit.php?id=<?php echo $row['id']; ?>">

              <span class="list-task-id" title="Task ID">
                <i class="fa fa-hashtag" aria-hidden="true"></i>
                <?php echo $row['id']; ?>
              </span>

              <span class="list-task-priority" title="Priority">
                <i class="fa fa-tasks" aria-hidden="true"></i>
                <?php echo $row['priority']; ?>
              </span>

              <span class=" hide-for-small-only list-task-date" title="Creation date : <?php echo $row['date'];?>">
                <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
                <?php echo getRelativeTime($row['date']); ?>
              </span>

              <span class=" list-task-due" title="Due date : <?php echo $row['due'];?>">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>

                <?php echo getRelativeTime($row['due']); ?>
              </span>

              <span class=" hide-for-small-only list-task-creator" title="Creator : <?php echo $row['creator'];?>">
                <i class="fa fa-plus icosm" aria-hidden="true"></i><i> </i>
                <i class="fa fa-user" aria-hidden="true"></i>
                <?php echo $row['creator']; ?>
              </span>

              <span class=" hide-for-small-only list-task-assignee" title="Assignee : <?php echo $row['assignee'];?>">
                <i class="fa fa-wrench icosm" aria-hidden="true"></i><i> </i>
                <i class="fa fa-user" aria-hidden="true"></i>
                <?php echo $row['assignee']; ?>
              </span>

              <span class=" hide-for-small-only list-task-executor" title="Done by : <?php echo $row['executor'];?>">
                <i class="fa fa-check icosm" aria-hidden="true"></i><i> </i>
                <i class="fa fa-user" aria-hidden="true"></i>
                <?php echo $row['executor']; ?>
              </span>

            </a>
          </div>
            <div class=" list-task-delete" data-id="<?php echo $row['id']; ?>">
              <a href="#">
                <i class="fa fa-trash-o"></i>
              </a>
            </div>

          </li>

        <?php endForeach; ?>
    </ul>

<a id="btn-add" href="edit.php"><i class="fa fa-plus fa-2x"></i></a>
