
<?php
$stmt = $db->prepare($q_usrselect);
$stmt->execute();
$user= $stmt->fetchAll();
?>


<select class="form-edit-input" name="assigned_to"  id="assigned_to">
  <?php foreach ($user as $row): ?>

    <?php if(isset($assigned_to)) : ?>
      <option <?php echo $assigned_to==$row['id']?'selected':'';?> value="<?php echo $row['id'];?>" > <?php echo $row['name'];?> </option>
    <?php else: ?>
      <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?> </option>
    <?php endif; ?>

  <?php endForeach; ?>
</select>
