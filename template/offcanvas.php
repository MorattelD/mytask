<h2 class="sbtitle">Status</h2>
<ul>
  <li><a class="sb-status <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'all')?'selected':''; ?>" data-status="all">Everything</a></li>
  <li><a class="sb-status <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'open')?'selected':''; ?>" data-status="open">Open</a></li>
  <li><a class="sb-status <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'close')?'selected':''; ?>" data-status="close">Close</a></li>
</ul>

<h2 class="sbtitle">Filters</h2>
<ul>
  <li><a class="sb-filter <?php echo (isset($_SESSION['filter']) && $_SESSION['filter'] == 'all')?'selected':''; ?>" data-filter="all">Everything</a></li>
  <li><a class="sb-filter <?php echo (isset($_SESSION['filter']) && $_SESSION['filter'] == 'assigned')?'selected':''; ?>" data-filter="assigned">Assigned to me</a></li>
  <li><a class="sb-filter <?php echo (isset($_SESSION['filter']) && $_SESSION['filter'] == 'done')?'selected':''; ?>" data-filter="done">Done by me</a></li>
  <li><a class="sb-filter <?php echo (isset($_SESSION['filter']) && $_SESSION['filter'] == 'created')?'selected':''; ?>" data-filter="created">Created by me</a></li>
</ul>

<h2 class="sbtitle">Ordering</h2>
<ul>

  <li><a class="sb-sort <?php echo (isset($_SESSION['sort']) && $_SESSION['sort'] == 'assignee')?'selected':''; ?>" data-sort="assignee" data-order="<?php echo (isset($_SESSION['order']))?$_SESSION['order']:'asc'; ?>" >Assignee <i class="fa fa-sort-asc "></i><i class="fa fa-sort-desc "></i></a></li>
  <li><a class="sb-sort <?php echo (isset($_SESSION['sort']) && $_SESSION['sort'] == 'creator')?'selected':''; ?>" data-sort="creator" data-order="<?php echo (isset($_SESSION['order']))?$_SESSION['order']:'asc'; ?>">Creator <i class="fa fa-sort-asc "></i><i class="fa fa-sort-desc "></i></a></li>
  <li><a class="sb-sort <?php echo (isset($_SESSION['sort']) && $_SESSION['sort'] == 'status')?'selected':''; ?>" data-sort="status" data-order="<?php echo (isset($_SESSION['order']))?$_SESSION['order']:'asc'; ?>">Status <i class="fa fa-sort-asc "></i><i class="fa fa-sort-desc "></i></a></li>
  <li><a class="sb-sort <?php echo (isset($_SESSION['sort']) && $_SESSION['sort'] == 'due')?'selected':''; ?>" data-sort="due" data-order="<?php echo (isset($_SESSION['order']))?$_SESSION['order']:'asc'; ?>">Due date <i class="fa fa-sort-asc "></i><i class="fa fa-sort-desc "></i></a></li>
  <li><a class="sb-sort <?php echo (isset($_SESSION['sort']) && $_SESSION['sort'] == 'date')?'selected':''; ?>" data-sort="date" data-order="<?php echo (isset($_SESSION['order']))?$_SESSION['order']:'asc'; ?>">Created date <i class="fa fa-sort-asc "></i><i class="fa fa-sort-desc "></i></a></li>
</ul>

<h2 class="sbtitle"> </h2>
<ul >
  <li><a class="sb-link-bottom" href="index.php?action=reset" >Reset filters</a></li>
</ul>

<h2 class="sbtitle"> </h2>
<ul >
  <li><a class="sb-link-bottom" href="index.php?action=about">About</a></li>
  <li><a class="sb-link-bottom" href="index.php?action=cv">Personal presentation</a></li>
  <li><a class="sb-link-bottom" href="index.php?action=manual">User manual</a></li>
</ul>
